angular.element(document).ready(function() {
  angular.bootstrap(document.getElementById('edit-form'));
});


IssuesCtrl = function($scope, $http) {
  $scope.listReady = false;
  $scope.listError = false;

  $scope.issues = [];

  $scope.newIssue = {
  };

  $scope.addIssue = function() {
    $scope.issues.push({
      title: $scope.newIssue.title,
      hours: $scope.newIssue.hours,
      minutes: $scope.newIssue.minutes,
      seconds: $scope.newIssue.seconds
    });
    $scope.newIssue.title = '';
  };

  $scope.removeIssue = function(issue) {
    $scope.issues.splice($scope.issues.indexOf(issue), 1);
  };

  $scope.loadIssues = function(){
    $http.get('/get.json')
    .success(function(data) {
      $scope.listReady = true;
      $scope.issues = data;
    })
    .error(function() {
      $scope.listError = true;
    });
  };

  $scope.saveIssues = function() {
    $scope.listReady = false;
    $http.post('/set.json', $scope.issues)
    .success(function(data) {
      $scope.listReady = true;
      $scope.issues = data;
    })
    .error(function() {
      $scope.listError = true;
    });

  };

  $scope.loadIssues();

};